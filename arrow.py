import ROOT
class Arrow(ROOT.TArrow):
    def __init__(self, x1, y1, x2, y2, size, option):
        super(Arrow, self).__init__(x1, y1, x2, y2, size, option)
        self.zIndex = 1
    def draw(self, **kwargs):
        self.Draw()
