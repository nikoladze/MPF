#!/usr/bin/env python

import os
import subprocess

# scripts in examples
scriptDict = {
    "plot.py" : ("plot.pdf", "plot.png"),
    "dataMCRatioPlot.py" : ("plot.pdf", "dataMCRatioPlot.png"),
    "bgContributionPlot.py" : ("plot.pdf", "bgContributionPlot.png"),
    "significanceScanPlot.py" : ("plot.pdf", "significanceScanPlot.png"),
    "signalRatioPlot.py" : ("plot.pdf", "signalRatioPlot.png"),
    "efficiencyPlot.py" : ("plot.pdf", "efficiencyPlot.png"),
    "treePlotter.py" : ("plot.pdf", "treePlotter.png"),
}

if not os.path.exists("images"):
    os.mkdir("images")

for scriptName, (output, target) in scriptDict.items():
    subprocess.call(os.path.join("../examples", scriptName))
    subprocess.call(["pdfcrop", output, output])
    subprocess.call(["convert", "-density", "150", output, os.path.join("images", target)])
    os.unlink(output)
