#!/usr/bin/env/python
"""
Plot the ratio of histograms where the numerator is assumed to be filled
with a subset of the events of the denominator.

The first registered histogram is the denominator, all further
histograms are treated as numerators. 

Example
--------

.. literalinclude:: /../examples/efficiencyPlot.py

.. image::  images/efficiencyPlot.png
   :width: 600px

"""

from .plotStore import PlotStore

class EfficiencyPlot(PlotStore):

    """

    :param doDataMCRatio: if both data and MC is added, also plot the data/MC ratio (default: False)
    :param ratioModeDataMC: ratioMode if data/MC is to be drawn default: "hist"
    :param ratioTitle: Title for the ratio pad if data/MC ratio is shown (default: "Data / MC")


    Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore` parameters:

    :param ratioMode: default: "binomial"
    :param drawRatioLine: default: False
    :param yTitle: default: #epsilon

    For further options see :py:meth:`~MPF.plotStore.PlotStore`
    """

    def __init__(self,
                 ratioTitle="Data / MC",
                 **kwargs):
        self.doDataMCRatio = kwargs.pop("doDataMCRatio", False)
        self.ratioModeDataMC = kwargs.pop("ratioModeDataMC", "hist")
        if self.doDataMCRatio:
            splitting = "ratio"
        else:
            splitting = None
        self.ratioTitle = ratioTitle
        super(EfficiencyPlot, self).__init__(ratioMode=kwargs.pop("ratioMode", "binomial"),
                                             drawRatioLine=kwargs.pop("drawRatioLine", False),
                                             yTitle=kwargs.pop("yTitle", "#epsilon"),
                                             splitting=splitting,
                                             **kwargs)

    def registerHist(self, hist, **kwargs):
        """
        Overwrites the defaults for the following :py:meth:`~MPF.plotStore.PlotStore.registerHist` parameters:

        :param hide: default: True
        :param style: default: "signal"
        :param markerStyle: default: 20

        For further options see :py:meth:`~MPF.plotStore.PlotStore.registerHist`
        """
        super(EfficiencyPlot, self).registerHist(hist,
                                                 hide=kwargs.pop("hide", True),
                                                 style=kwargs.pop("style", "signal"),
                                                 markerStyle=kwargs.pop("markerStyle", 20),
                                                 **kwargs)

    def saveAs(self, path, **kwargs):
        self.buildMainPad(**kwargs)

        self.addSignalRatios(self.canvas.pads['main'], addLegend=True, register=True, applyRatioLimits=False)
        if len(list(self.getHists("data"))) > 0:
            self.addSignalRatios(self.canvas.pads['main'], addLegend=True, register=True, style="data", applyRatioLimits=False)
            if self.doDataMCRatio:
                self.addDataMCRatio(self.canvas.pads['bottom'], ratioMode=self.ratioModeDataMC, drawRatioLine=True)
                self.canvas.pads['bottom'].yTitle = self.ratioTitle

        return super(EfficiencyPlot, self).saveAs(path, **kwargs)
