#!/usr/bin/env python

import ROOT
from MPF.efficiencyPlot import EfficiencyPlot
from MPF.atlasStyle import setAtlasStyle

setAtlasStyle()

passed = ROOT.TH1F("passed", "", 50, 100, 300)
total = ROOT.TH1F("total", "", 50, 100, 300)
passed.Sumw2()
total.Sumw2()
for i in range(10000):
    val = ROOT.gRandom.Gaus(100, 200)
    shift = ROOT.gRandom.Gaus(0, 20)
    total.Fill(val)
    if (val+shift) > 200:
        passed.Fill(val)

p = EfficiencyPlot(ratioMode="bayes")
p.registerHist(total)
p.registerHist(passed, legendTitle="Efficiency")
p.saveAs("plot.pdf")
